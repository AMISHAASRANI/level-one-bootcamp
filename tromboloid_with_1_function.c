//Program to calculate volume of tromboloid given h,d,b.
#include <stdio.h>
int main()
{
    float h,d,b,volume;      
    printf("\nenter the values for h , d and b:");
    scanf("%f%f%f",&h,&d,&b);
    volume=(((h*d*b)+(d/b))/3);
    printf("the volume of tromboloid is %.2f cubic unit\n",volume);
    return 0;
}